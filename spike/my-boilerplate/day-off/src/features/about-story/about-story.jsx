import React from 'react'
import { useSelector } from 'react-redux'

export default function AboutStory() {
  const count = useSelector(state => state.count)

  return (
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent placerat
      massa purus, in faucibus diam aliquet vulputate. Morbi ut nunc quis felis
      aliquam imperdiet et eu sem. Pellentesque facilisis sapien massa, ac
      lacinia leo varius vitae. Integer et turpis porta eros dignissim euismod.
      Proin purus purus, eleifend nec risus at, cursus vulputate libero. Sed
      interdum interdum tortor, non accumsan ligula rhoncus vitae. Phasellus
      commodo ex nec pretium faucibus. Nullam ipsum ex, ornare ut dolor a,
      consectetur volutpat ex. Sed a ultrices lorem. Cras malesuada urna velit,
      at varius mauris dignissim vel. Fusce in quam felis. Proin sollicitudin at
      dolor pellentesque aliquet.
      <br/>
      <span> {count} </span>
      <br/>
      Morbi commodo arcu magna, sed varius leo vulputate et. Aliquam sed lacus
      venenatis, volutpat justo ac, maximus felis. Vestibulum eu cursus quam.
      Fusce quis molestie purus. Maecenas porttitor lobortis eros et posuere.
      Vestibulum consequat consectetur suscipit. Maecenas aliquet commodo
      consectetur. Nulla bibendum finibus enim ac varius. Morbi dignissim
      feugiat metus vel gravida. Integer efficitur, nisi in maximus malesuada,
      ligula nunc bibendum orci, sed molestie nulla turpis quis lectus.
      Curabitur quis sem viverra, molestie libero ut, vulputate magna. Sed ut
      consectetur elit. Vivamus pulvinar dictum velit, ut ultrices lacus luctus
      sit amet. Duis imperdiet felis quis nisl volutpat maximus.
    </p>
  )
}
