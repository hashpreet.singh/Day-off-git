import React from 'react'
import { waitForElement } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import wrap from '../../test/tools/Wrap'
import AboutStory from './about-story'

test('loads and displays greeting', async () => {
  const { getByText } = wrap(<AboutStory />)

  const p = await waitForElement(() =>
    getByText('Lorem ipsum', { exact: false })
  )

  expect(p).toBeVisible()
})
