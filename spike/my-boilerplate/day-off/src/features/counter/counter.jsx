import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import * as actions from '../../core/actions/counter-actions'
import {Button,H2,Span,colorchanger,theme} from '../../core/styledcomponents'

export default function Counter() {
  const count = useSelector(state => state.count)
  const times = useSelector(state => state.times)
  const dispatch = useDispatch()

  const broadcast = action => () => dispatch(action())

  return (
    <div>
      <H2>Redux counter: Try it!</H2>
      <Button onClick={broadcast(actions.doubleDecrement)}> -- </Button>
      <span> </span>
      <Button onClick={broadcast(actions.decrement)}> - </Button>
      <Span> {count} </Span>
      <Span> ({times} times)</Span>
      <Button onClick={broadcast(actions.increment)}> + </Button>
      <span> </span>
      <Button onClick={broadcast(actions.doubleIncrement)}> ++ </Button>
      <span> </span>
      <Button onClick={broadcast(actions.previousOdd)}> previous odd </Button>
      <span> </span>
      <Button onClick={broadcast(actions.resetTimes)}> reset times </Button>
      <span> </span>
      <Button onClick={broadcast(actions.reset)}> RESET </Button>
      <span></span>
    </div>
  )
}
