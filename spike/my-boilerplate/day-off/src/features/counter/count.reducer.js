import initialState from '../../app/initial-state'
import * as actions from '../../core/actions/counter-actions'

export default function reducer(state = initialState, action) {
  const setCount = value => ({
    ...state,
    count: value,
  })
  const add = toAdd => setCount(state.count + toAdd)

  switch (action.type) {
    case actions.RESET:
      return setCount(0)
    case actions.INCREMENT:
      return add(1)
    case actions.DECREMENT:
      return add(-1)
    case actions.DOUBLEINCREMENT:
      return add(2)
    case actions.DOUBLEDECREMENT:
      return add(-2)
    default:
      return state
  }
}
