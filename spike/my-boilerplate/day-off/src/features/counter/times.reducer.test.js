import sut from "./times.reducer"
import * as actions from "../../core/actions/counter-actions"
import { testReducer } from "../../test/tools/test-reducer"

describe("general usage", () => {
  const workbench = testReducer(test, sut, expect)
  workbench.shouldReturnState() 

  const cases = [
    [actions.resetTimes(), 10, 0],
    [actions.incrementTime(), 1, 2],
    [actions.reset(), 100, 0]
  ]

  workbench.caseWhen(cases, value => ({ times: value}))

})
