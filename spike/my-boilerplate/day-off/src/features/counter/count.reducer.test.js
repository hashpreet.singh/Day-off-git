import sut from "./count.reducer"
import * as actions from "../../core/actions/counter-actions"
import { testReducer } from "../../test/tools/test-reducer"

describe("general usage", () => {
  const workbench = testReducer(test, sut, expect)
  workbench.shouldReturnState()

  const cases = [
    [actions.increment(), 1, 2],
    [actions.decrement(), 1, 0],
    [actions.doubleIncrement(), 1, 3],
    [actions.doubleDecrement(), 1, -1],
    [actions.reset(), 100, 0]
  ]
  
  workbench.caseWhen(cases, value => ({ count: value}))
})

