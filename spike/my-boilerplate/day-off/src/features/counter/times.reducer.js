import initialState from '../../app/initial-state'
import * as actions from '../../core/actions/counter-actions'

export default function reducer(state = initialState, action) {
  const setTimes = value => ({
    ...state,
    times: value,
  })

  switch (action.type) {
    case actions.RESET:
      return setTimes(0)
    case actions.RESET_TIMES:
      return setTimes(0)
    case actions.INCREMENT_TIME:
      return setTimes(state.times + 1)
    default:
      return state
  }
}
