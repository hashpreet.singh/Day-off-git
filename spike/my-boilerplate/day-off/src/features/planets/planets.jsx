import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import * as actions from './duck.planets'
import styled from 'styled-components'
import {H2,Button,Span,P,Div} from '../../core/styledcomponents'
export default function Planets() {
  const count = useSelector(state => state.count)
  const planetName = useSelector(state => state.planets.planetName)
  const isLoading = useSelector(state => state.planets.fetching)
  const dispatch = useDispatch()

  const broadcast = action => () => dispatch(action(count))

  const MyTitle = styled.span`
  font-size: 1.5em;
  text-align: center;
  color: palevioletred;
`

  return (
    <Div>
      <H2>Based on actual count load the StarWar planet</H2>
      <P>
      <Button onClick={broadcast(actions.planetName)}> Load planet! </Button>
      </P>
      <Span> </Span>
      <MyTitle>
        The name of the planet is: <b data-testid="planetName">{planetName}</b>
      </MyTitle>
      <br/>
      <Span >{isLoading ? 'LOADING PLANEEEET' : ''}</Span>
    </Div>
  )
}
