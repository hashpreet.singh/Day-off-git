import initialState from '../../app/initial-state'
import * as rest from '../../core/actions/rest-actions'

export const PLANETNAME = 'planets/PLANETNAME'

export default function reducer(state = initialState, action) {
  const toggle = fetching => ({
    ...state,
    planets: {
      ...state.planets,
      fetching,
    },
  })

  const setPlanet = name => ({
    ...state,
    planets: {
      ...state.planets,
      planetName: name,
    },
  })

  switch (action.type) {
    case rest.started(PLANETNAME):
      return toggle(true)
    case rest.ended(PLANETNAME):
      return toggle(false)
    case rest.success(PLANETNAME):
      return setPlanet(action.data.data.name)
    case rest.failure(PLANETNAME):
      return setPlanet(`--- there was an error: ${action.data.message}`)
    default:
      return state
  }
}

export const planetName = id => rest.get(PLANETNAME, `https://swapi.co/api/planets/${id}`)
