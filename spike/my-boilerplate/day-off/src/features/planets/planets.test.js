import React from "react"
import "@testing-library/jest-dom/extend-expect"
import userEvent from "@testing-library/user-event"
import wrap from "../../test/tools/Wrap"
import Planets from "./planets"

test("testing async stuff!!!", done => {
  const allActions = []

  const checkState = state => {
    expect(state.planets.planetName).toEqual("Sun")
  }

  const checkActions = actions => {
    const expected = [
      "planets/PLANETNAME",
      "rest/BEGINREQUEST",
      "planets/PLANETNAME/started",
      "planets/PLANETNAME/success",
      "planets/PLANETNAME/ended",
      "rest/ENDREQUEST"
    ]
    expect(actions.map(e => e.type)).toEqual(expected)
  }

  const { getByText, getByTestId, getState, getActions } = wrap(<Planets />, {
    rest: {
      responses: [{ data: { data: { name: "Sun" } } }],
      done: () => {
        checkState(getState())
        checkActions(getActions())
        expect(getByTestId("planetName").innerHTML).toEqual("Sun")
        done()
      }
    },
    actionStorage: allActions
  })

  userEvent.click(getByText("Load planet!"))
})
