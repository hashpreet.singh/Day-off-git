import React from 'react'
import { useDispatch } from 'react-redux'
import * as actions from '../../core/actions/dump-actions'
import download, { getFileName } from '../../core/classes/downloader'
import {Button,Div} from '../../core/styledcomponents'
export default function DumpButton() {
  const dispatch = useDispatch()

  const broadcast = action => () => {
    const result = dispatch(action())
    download(getFileName(new Date(), 'state.json'), JSON.stringify(result.dump))
    return result
  }

  return (
    <Div>
      <Button onClick={broadcast(actions.dump)}> DUMP STATE </Button>
    </Div>
  )
}
