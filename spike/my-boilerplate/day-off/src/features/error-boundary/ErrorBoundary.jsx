import React from 'react'
import { connect } from 'react-redux'

class ErrorBoundary extends React.Component {
  count = 0

  constructor(props) {
    super(props)
    this.state = { error: null, errorInfo: null }
  }

  componentDidMount() {
    window.addEventListener('error', err => {
      if (err.error.hasBeenCaught !== undefined) {
        return false
      }
      /* eslint-disable-next-line no-param-reassign */
      err.error.hasBeenCaught = true
      this.count += 1
      console.log(
        `erroreeeeeeeeeeeeeeeeeeeeeeeee:${err.error} + ${err.message}`
      )
      this.setState({
        error: err.message,
        errorInfo: err.error
      })
      return true
    })
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo
    })
    console.log(`hhhhhhhhhhhhhhhhhh: ${errorInfo}`)
    // You can also log error messages to an error reporting service here
  }

  render() {
    const { error, errorInfo } = this.state
    if (errorInfo) {
      // Error path
      return (
        <div>
          <h2>Something went wrong.</h2>
          <details style={{ whiteSpace: 'pre-wrap' }}>
            {error && error.toString()}
            <br/>
            {errorInfo.componentStack}
          </details>
        </div>
      )
    }
    // Normally, just render children
    const { children } = this.props
    return children
  }
}

function mapStateToProps(state) {
  return {
    error: state.error,
    errorInfo: state.errorInfo
  }
}

export default connect(mapStateToProps)(ErrorBoundary)
