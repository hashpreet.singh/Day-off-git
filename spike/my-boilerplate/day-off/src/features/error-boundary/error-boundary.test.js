import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import wrap from '../../test/tools/Wrap'
import ErrorBoundary from './ErrorBoundary'
import { waitForElement } from '@testing-library/react'

export function BuggyComponent(shouldExplode) {
  if (shouldExplode) throw Error('aha!')
  return <p>hello world</p>
}

//https://www.google.com/search?q=jest+errorboundary&rlz=1C1GCEU_itIT822IT822&oq=jest+errorboundary&aqs=chrome..69i57.17103j0j7&sourceid=chrome&ie=UTF-8

// 0 => dato un ErrorBoundary, renderizzo un componente, => mi aspetto che venga renderizzato il componente

test('testing ErrorBoundary!', async () => {
  const { getByText } = wrap(
    <ErrorBoundary>{BuggyComponent(false)}</ErrorBoundary>
  )

  const p = await waitForElement(() =>
    getByText('hello world', { exact: false })
  )

  expect(p).toBeVisible()
})
