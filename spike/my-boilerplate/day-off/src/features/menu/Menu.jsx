import React from 'react'
import {
  BrowserRouter as Router, Switch, Route, Link,
} from 'react-router-dom'
import clock from '../../core/classes/clock'
import AboutStory from '../about-story/about-story'
export default function Menu() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/home">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/users">Users</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}

        <Switch>
          <Route path="/about">
            <About/>
            <AboutStory/>
          </Route>
          <Route path="/users">
            <Users/>
          </Route>
          <Route path="/">
            <Home/>
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

function Home() {
  return <h2>Home</h2>
}

function About() {
  return <h2>About</h2>
}

function Users() {
  const today = clock(new Date()).dayOfMonth()
  return <h2>Hey Man, today is {today}</h2>
}
