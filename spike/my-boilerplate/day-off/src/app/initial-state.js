const initialState = {
  count: 0,
  times: 0,
  planets: { fetching: false, planetName: '' },
}

export default initialState
