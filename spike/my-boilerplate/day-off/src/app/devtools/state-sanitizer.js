export default function stateSanitizer(state) {
  return state.someProperty ? { ...state, data: '<<LONG_BLOB>>' } : state
}
