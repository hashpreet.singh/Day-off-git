import { compose } from 'redux'
import actionSanitizer from './action-sanitizer'
import stateSanitizer from './state-sanitizer'

// see: https://medium.com/@zalmoxis/using-redux-devtools-in-production-4c5b56c5600f

export default function composeEnhancers(reducers) {
  const devToolsOptions = {
    trace: true,
    actionSanitizer,
    stateSanitizer,
  }

  const builder = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(devToolsOptions)
    : compose

  return builder(reducers)
}
