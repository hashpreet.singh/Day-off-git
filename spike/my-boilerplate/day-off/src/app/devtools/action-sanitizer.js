export default function actionSanitizer(action) {
  return action &&
    action.type &&
    action.type === 'FILE_DOWNLOAD_SUCCESS' &&
    action.data
    ? { ...action, data: '<<LONG_BLOB>>' }
    : action
}
