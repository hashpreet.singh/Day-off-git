import { createStore, applyMiddleware, compose } from 'redux'
import restMiddleware from './middlewares/rest-middleware'
import actionLogger from './middlewares/action-logger-middleware'
import businessLogic from './middlewares/business-logic-middleware'
import handlers from '../core/handlers'
import rootReducer from './root-reducer'
import initialState from './initial-state'

function build(config) {
  const cfg = config || {}
  cfg.composeEnhancers = cfg.composeEnhancers || compose
  const reducers = cfg.rootReducer ? cfg.rootReducer : rootReducer

  const store = createStore(
    reducers,
    initialState,
    cfg.composeEnhancers(
      applyMiddleware(
        restMiddleware.withRestAdapter(cfg.adapter),
        actionLogger.setStorage(cfg.actionStorage),
        businessLogic.withHandlers(handlers)
      )
    )
  )

  return store
}

const store = build()
store.withConfig = build

export default store
