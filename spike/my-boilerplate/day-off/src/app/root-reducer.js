import reduceReducers from 'reduce-reducers'
import counter from '../features/counter/count.reducer'
import times from '../features/counter/times.reducer'
import planets from '../features/planets/duck.planets'

const rootReducer = reduceReducers(counter, times, planets)

export default rootReducer
