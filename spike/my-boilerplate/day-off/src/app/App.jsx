import React from "react"
import { Provider } from "react-redux"
import axios from "axios"
import store from "./store"
import devToolsComposer from "./devtools/dev-tools"
import Menu from "../features/menu/Menu"
import Counter from "../features/counter/counter"
import Planets from "../features/planets/planets"
import DumpButton from "../features/dump/DumpButton"
import ErrorBoundary from "../features/error-boundary/ErrorBoundary"
import "./styles.css"
import { ThemeProvider } from "styled-components"
import { H1, H2, theme, Div, Button } from "../core/styledcomponents"
import { setColor } from "../core/styledactions"
export default function App() {
  const config = {
    adapter: axios,
    composeEnhancers: devToolsComposer
  }

  const theStore = store.withConfig(config)
  const colorNew = () => {
    console.info('calleeeeeeeeeed') 
    setColor("red")
  }

  return (
    <Provider store={theStore}>
      <ErrorBoundary>
        <ThemeProvider theme={theme}>
          <Div>
            <Button onClick={colorNew}>aaaaa</Button>
            <div className="App">
              <H1>Hello CodeSandbox</H1>
              <H2>Start editing to see some magic happen!</H2>
            </div>
            <Menu />
            <Counter />
            <Planets />
            <DumpButton />
          </Div>
        </ThemeProvider>
      </ErrorBoundary>
    </Provider>
  )
}
