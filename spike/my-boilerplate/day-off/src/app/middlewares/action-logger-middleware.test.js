import sut from './action-logger-middleware'
import * as actions from '../../core/actions/dump-actions'

function createStrore(state = { someProp: 1 }) {
  const store = {}
  store.getState = jest.fn(a => state)
  return store
}

test('should add all actions to the storage', () => {
  const storage = []
  const action = { type: 'something' }
  const next = jest.fn(a => {})

  sut.setStorage(storage)(createStrore())(next)(action)

  expect(next).toHaveBeenCalledWith(action)
  expect(storage).toMatchObject([{ type: 'something' }])
})

describe('should intercept "' + actions.DUMP + '"', () => {
  test('add all actions to the storage', () => {
    const action = { type: 'something' }
    const next = jest.fn(a => {})
    const state = { someProp: 1 }
    const store = createStrore(state)
    const middleware = sut.setStorage([], state)

    middleware(store)(next)(action)

    expect(next).toHaveBeenCalledWith(action)

    const dump = actions.dump()

    middleware(store)(next)(dump)

    expect(next).toHaveBeenCalledWith(dump)

    expect(dump).toMatchObject({
      type: actions.DUMP,
      dump: {
        payload: '[{"type":"something"},{"type":"dump/DUMP"}]',
        preloadedState: '{"someProp":1}'
      }
    })
  })

  test('always returns initialState', () => {
    const action = { type: 'something' }
    const next = jest.fn(a => {})
    const state = { someProp: 1 }
    const store = createStrore(state)
    const middleware = sut.setStorage([], state)

    middleware(store)(next)(action)

    state.someProp = 20

    const dump = actions.dump()

    middleware(store)(next)(dump)

    expect(dump).toMatchObject({
      type: actions.DUMP,
      dump: {
        payload: '[{"type":"something"},{"type":"dump/DUMP"}]',
        preloadedState: '{"someProp":1}'
      }
    })
  })
})
