function createMiddleware(handlers = {}) {
  const businessLogic = store => next => action => {
    const handler = handlers[action.type]
    if (handler) {
      handler(action, store.getState, store.dispatch)
    }

    return next(action)
  }

  return businessLogic
}

const businessLogic = createMiddleware()
businessLogic.withHandlers = createMiddleware

export default businessLogic
