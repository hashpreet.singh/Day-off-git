import sut from "./business-logic-middleware"

const store = {}
store.getState = jest.fn(a => {})
store.dispatch = jest.fn(a => {})

describe("basic usage", () => {
  test("should broadcast everything without defined handler", () => {
    const action = { type: "something" }
    const next = jest.fn(a => {})

    sut(store)(next)(action)

    expect(next).toHaveBeenCalledWith(action)
  })

  test("should do something with a defined handler", () => {
    const action = { type: "something" }
    const next = jest.fn(a => {})

    const handlers = {}
    handlers["something"] = jest.fn((a, s, d) => {})

    sut.withHandlers(handlers)(store)(next)(action)

    expect(handlers["something"]).toHaveBeenCalledWith(action, store.getState, store.dispatch)
    expect(next).toHaveBeenCalledWith(action)
  })
})
