import * as actions from '../../core/actions/rest-actions'

export function onSuccess(action, response, dispatch) {
  dispatch(actions.build(action, actions.success, response))
}

export function onError(action, error, dispatch) {
  const restError = actions.restError(action, error)
  dispatch(actions.build(action, actions.failure, error))
  dispatch(restError)
}

export function endRequest(action, dispatch, adapter) {
  dispatch(actions.build(action, actions.ended))
  dispatch(actions.endRequest(action))
  if (adapter.done) adapter.done()
}

function createMiddleware(adapter) {
  const restMiddleware = store => next => action => {
    if (!action.meta || !action.meta.rest) {
      return next(action)
    }
    const config = action.meta.rest

    if (!adapter) {
      return next(action)
    }

    const result = next(action)
    const { dispatch } = store

    dispatch(actions.beginRequest(action))
    dispatch(actions.build(action, actions.started))

    const doWork = adapter[config.method.toLowerCase()]

    doWork(config.url)
      .then(response => onSuccess(action, response, dispatch))
      .catch(error => onError(action, error, dispatch))
      .then(() => endRequest(action, dispatch, adapter))

    return result
  }

  return restMiddleware
}

const rest = createMiddleware()
rest.withRestAdapter = createMiddleware

export default rest
