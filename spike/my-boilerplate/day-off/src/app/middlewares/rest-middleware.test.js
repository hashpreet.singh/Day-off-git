import sut, { onSuccess, onError, endRequest } from "./rest-middleware"
import * as actions from "../../core/actions/rest-actions"
import getAdapter from "../../test/rest-adapter"

const store = {}

describe("ignore", () => {
  test("without 'meta' property", () => {
    const action1 = {}
    const action2 = { some: "property", someOther: "property" }
    const next = jest.fn(a => {})

    sut(store)(next)(action1)
    expect(next).toHaveBeenCalledWith(action1)

    sut(store)(next)(action2)
    expect(next).toHaveBeenCalledWith(action2)
    expect(next).toHaveBeenCalledTimes(2)
  })

  test("without 'meta.rest' property ", () => {
    const action = { meta: "wrong stuff" }
    const next = jest.fn(a => {})

    sut(store)(next)(action)

    expect(next).toHaveBeenCalledWith(action)
    expect(next).toHaveBeenCalledTimes(1)
  })

  test("without a rest adapter ", () => {
    const action = { meta: { rest: "something" } }
    const next = jest.fn(a => {})

    sut(store)(next)(action)

    expect(next).toHaveBeenCalledWith(action)
    expect(next).toHaveBeenCalledTimes(1)
  })
})

describe("basic usage", () => {
  test("on success dispatch the correct action", () => {
    const action = actions.get("SOMETYPE", "some/url")
    const dispatch = jest.fn(a => {})
    const res = { data: "something" }
    const expected = { type: "SOMETYPE/success", data: res, action: action }

    onSuccess(action, res, dispatch)

    expect(dispatch).toHaveBeenCalledWith(expected)
    expect(dispatch).toHaveBeenCalledTimes(1)
  })

  test("on failure dispatch the correct action", () => {
    const action = actions.get("SOMETYPE", "some/url")
    const dispatch = jest.fn(a => {})
    const error = { message: "very bad error here!" }
    const expected = { type: "SOMETYPE/failure", data: error, action: action }
    const expectedRestError = {
      type: "rest/ERROR",
      action: action,
      error: error
    }

    onError(action, error, dispatch)

    expect(dispatch).toHaveBeenCalledWith(expected)
    expect(dispatch).toHaveBeenCalledWith(expectedRestError)
    expect(dispatch).toHaveBeenCalledTimes(2)
  })

  test("at the end dispatch the correct action", () => {
    const action = actions.get("SOMETYPE", "some/url")
    const dispatch = jest.fn(a => {})
    const expected = { type: "SOMETYPE/ended", action: action }
    const expectedEndRequest = {
      type: "rest/ENDREQUEST",
      action: action
    }

    endRequest(action, dispatch, { })

    expect(dispatch).toHaveBeenCalledWith(expected)
    expect(dispatch).toHaveBeenCalledWith(expectedEndRequest)
    expect(dispatch).toHaveBeenCalledTimes(2)
  })

  test("it fires a beginRequest action after the original action", () => {
    const action = actions.request("some/url", "GET")
    const next = jest.fn(a => {})
    const theStore = { dispatch: jest.fn(a => {}) }
    const middleware = sut.withRestAdapter(
      getAdapter([{ error: false, data: "something" }])
    )

    middleware(theStore)(next)(action)

    const beginRequest = actions.beginRequest(action)

    expect(next).toHaveBeenCalledWith(action)
    expect(next).toHaveBeenCalledTimes(1)

    expect(theStore.dispatch).toHaveBeenCalledWith(beginRequest)
    expect(theStore.dispatch).toHaveBeenCalledWith(
      actions.build(action, actions.started)
    )
    expect(theStore.dispatch).toHaveBeenCalledTimes(2)
  })

})
