import dump from '../../core/classes/dump'
import * as actions from '../../core/actions/dump-actions'

function createMiddleware(storage) {
  const theStorage = storage || []
  let initialState = null

  const actionLogger = store => next => action => {
    if (!initialState) initialState = { ...store.getState() }

    theStorage.push(action)

    if (action.type === actions.DUMP) {
      // eslint-disable-next-line no-param-reassign
      action.dump = dump(theStorage, initialState)
    }

    return next(action)
  }

  return actionLogger
}

const actionLogger = createMiddleware([])
actionLogger.setStorage = createMiddleware

export default actionLogger
