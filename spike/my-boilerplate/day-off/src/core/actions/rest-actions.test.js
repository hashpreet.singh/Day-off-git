import * as actions from "./rest-actions"

test("it should contains meta.rest field", () => {
  const actual = actions.request("")

  expect(actual.meta).toBeDefined()
  expect(actual.meta.rest).toBeDefined()
})

describe("url", () => {
  test("it must contains an 'url' field", () => {
    const actual = actions.request("")

    expect(actual.meta.rest.url).toBeDefined()
  })

  test("'url' field must be a string", () => {
    const actual = actions.request("")

    expect(typeof actual.meta.rest.url).toBe("string")
  })

  test("first parameter of constructor fills url property", () => {
    const actual = actions.request("something")

    expect(actual.meta.rest.url).toBe("something")
  })

  test("heading '/' in 'url' will be trimmed", () => {
    const actual = actions.request("/someurl")

    expect(actual.meta.rest.url).toBe("someurl")
  })

  test("ten heading '//////////' in 'url' will be trimmed", () => {
    const actual = actions.request("//////////someurl/someother")

    expect(actual.meta.rest.url).toBe("someurl/someother")
  })

  test("inner '/' in 'url' shoul be leaved in place", () => {
    const actual = actions.request("/someurl/someother")

    expect(actual.meta.rest.url).toBe("someurl/someother")
  })
})

describe("method", () => {
  test("it must contains an 'method' field", () => {
    const actual = actions.request("", "")

    expect(actual.meta.rest.method).toBeDefined()
  })

  test("'method' field must be a string", () => {
    const actual = actions.request("", "")

    expect(typeof actual.meta.rest.method).toBe("string")
  })

  test("second parameter of constructor fills method property", () => {
    const actual = actions.request("something", "somemethod")

    expect(actual.meta.rest.method).toBe("somemethod")
  })
})

describe("get", () => {
  test("type is first parameter", () => {
    const actual = actions.get("SOMETYPE", "some-url")

    expect(actual.type).toBe("SOMETYPE")
  })
  test("url is second parameter", () => {
    const actual = actions.get("", "some-url")

    expect(actual.meta.rest.url).toBe("some-url")
  })

  test("method is always get", () => {
    const actual = actions.get("", "some-url")

    expect(actual.meta.rest.method).toBe("GET")
  })
})

describe("post", () => {
  test("type is first parameter", () => {
    const actual = actions.post("SOMETYPE", "some-url")

    expect(actual.type).toBe("SOMETYPE")
  })

  test("url is second parameter", () => {
    const actual = actions.post("", "some-url")

    expect(actual.meta.rest.url).toBe("some-url")
  })

  test("method is always post", () => {
    const actual = actions.post("", "some-url")

    expect(actual.meta.rest.method).toBe("POST")
  })
})

describe("begin request", () => {
  test("it should wrap the action passed in a beginRequest action", () => {
    const action = { something: "something" }

    const actual = actions.beginRequest(action)

    expect(actual.action).toMatchObject(action)
  })

  test("it contains type", () => {
    const action = { something: "something" }

    const actual = actions.beginRequest(action)

    expect(actual.type).toEqual(actions.BEGINREQUEST)
  })
})

describe("end request", () => {
  test("it should wrap the action passed in a endRequest action", () => {
    const action = { something: "something" }

    const actual = actions.endRequest(action)

    expect(actual.action).toMatchObject(action)
  })

  test("it contains type", () => {
    const action = { something: "something" }

    const actual = actions.endRequest(action)

    expect(actual.type).toEqual(actions.ENDREQUEST)
  })
})