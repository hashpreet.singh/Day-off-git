export const INCREMENT = 'counter/INCREMENT'
export const DECREMENT = 'counter/DECREMENT'
export const DOUBLEINCREMENT = 'counter/DOUBLEINCREMENT'
export const DOUBLEDECREMENT = 'counter/DOUBLEDECREMENT'
export const RESET = 'counter/RESET'
export const RESET_TIMES = 'counter/RESET_TIMES'
export const PREVIOUS_ODD = 'counter/PREVIOUS_ODD'
export const INCREMENT_TIME = 'counter/INCREMENT_TIME'

export const allIncrements = [INCREMENT, DECREMENT, DOUBLEINCREMENT, DOUBLEDECREMENT]

export const increment = () => ({ type: INCREMENT })
export const decrement = () => ({ type: DECREMENT })
export const doubleIncrement = () => ({ type: DOUBLEINCREMENT })
export const doubleDecrement = () => ({ type: DOUBLEDECREMENT })
export const reset = () => ({ type: RESET })
export const resetTimes = () => ({ type: RESET_TIMES })
export const previousOdd = () => ({ type: PREVIOUS_ODD })
export const incrementTime = () => ({ type: INCREMENT_TIME })
