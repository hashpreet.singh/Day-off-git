export const ERROR = 'error/ERROR'

export const error = (theError) => ({ type: ERROR, error: theError })
