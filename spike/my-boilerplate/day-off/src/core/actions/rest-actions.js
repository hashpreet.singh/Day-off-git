export const BEGINREQUEST = 'rest/BEGINREQUEST'
export const ENDREQUEST = 'rest/ENDREQUEST'
export const SUCCESS = 'rest/SUCCESS'
export const ERROR = 'rest/ERROR'

export function request(url, method) {
  return {
    meta: {
      rest: {
        url: url.replace(/^\/+/, ''),
        method,
      },
    },
  }
}

export const beginRequest = action => ({ type: BEGINREQUEST, action })
export const endRequest = action => ({ type: ENDREQUEST, action })

export const restSuccess = (action, result) => ({ type: SUCCESS, action, result })
export const restError = (action, error) => ({ type: ERROR, action, error })

export const started = actionName => `${actionName}/started`
export const success = actionName => `${actionName}/success`
export const failure = actionName => `${actionName}/failure`
export const ended = actionName => `${actionName}/ended`

export function build(action, state, data) {
  return { type: state(action.type), action, data }
}

export function get(type, url) {
  const { meta } = request(url, 'GET')
  return { type, meta }
}

export function post(type, url) {
  const { meta } = request(url, 'POST')
  return { type, meta }
}
