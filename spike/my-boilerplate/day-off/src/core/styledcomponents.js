import styled from "styled-components"
export const H1 = styled.h1`
  font-family: sans-serif;
  font-size: 3.5rem;
  color: red;
  text-align: ${props => props.theme.textalign};
`
export const theme = {
  textalign: "center",
  background: "black",
  fontfamily: "sans-serif",
  buttonRadius: "8px",
  buttonPadding: ".25em 1 em",
  color: "purple",
  buttonborder: "black"
}
export const H2 = styled.h2`
  text-align: left;
  font-style: italic;
  font-weight: bold;
  font-size: 2rem;
  color: darkkhaki;
  text-align: ${props => props.theme.textalign};
`
export const Span = styled.span`
  border-radius: ${props => props.theme.buttonRadius + 5};
  color: ${props => props.theme.color};
  font-family: ${props => props.theme.fontfamily};
  border: ${props => props.theme.buttonborder};
  padding: 2px;
  text-align: ${props => props.theme.textalign};
`
export const Button = styled.button`
  color: ${props => props.theme.color};
  font-family: ${props => props.theme.fontfamily};
  border-radius: ${props => props.theme.buttonRadius};
  border: 0.5px solid ${props => props.theme.buttonborder};
`
export const P = styled.p`
  text-align: ${props => props.theme.textalign};
`
export const Div = styled.div`
  text-align: ${props => props.theme.textalign};
`
export const Nav = styled.nav`
  text-align: ${props => props.theme.textalign};
  font-family: sans-serif;
`
export const Li = styled.li`
  text-align: center;
  font-family: sans-serif;
  color: darkkhaki;
`
export const Ul = styled.ul`
  text-align: ${props => props.theme.textalign};
  color: darkkhaki;
`
