import sut from "./handlers"
import * as incrementActions from "./actions/counter-actions"

const cases = [
  incrementActions.PREVIOUS_ODD,  
  ...incrementActions.allIncrements
]

describe("definition", () => {
  test.each(cases)(
    "should contains the property '%p' and should be a function",
    property => {
      const actual = sut[property]

      expect(actual).toBeDefined()
      expect(actual).toBeInstanceOf(Function)
    }
  )
})
