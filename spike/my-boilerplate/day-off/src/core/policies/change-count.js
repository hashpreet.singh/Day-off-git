import * as actions from '../actions/counter-actions'

export default function (action, getState, dispatch) {
  if (actions.allIncrements.indexOf(action.type) < 0) {
    return
  }

  dispatch(actions.incrementTime())
}
