import sut from "./change-count"
import * as actions from "../actions/counter-actions"

test("it should ignore other actions", () => {
  const dispatch = jest.fn(a => {})

  sut({ type: "something" }, () => {}, dispatch)

  expect(dispatch).toHaveBeenCalledTimes(0)
})

describe("'increment' reducer", () => {
  test.each(actions.allIncrements)(
    "given the action '%p' it should fire an '" + actions.INCREMENT_TIME + "'",
    actionType => {
      const dispatch = jest.fn(a => {})

      sut({ type: actionType }, () => {}, dispatch)

      expect(dispatch).toHaveBeenCalledWith(actions.incrementTime())
      expect(dispatch).toHaveBeenCalledTimes(1)
    }
  )
})
