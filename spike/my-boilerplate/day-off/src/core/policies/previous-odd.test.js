import sut from "./previous-odd"
import * as actions from "../actions/counter-actions"

test("it should not change count if is odd", () => {
  const state = { count: 2 }
  const dispatch = jest.fn(a => {})

  sut({}, () => state, dispatch)

  expect(dispatch).not.toHaveBeenCalledWith(actions.decrement())
})

test("it should decrement if count if is even", () => {
  const state = { count: 1 }
  const dispatch = jest.fn(a => {})

  sut({}, () => state, dispatch)

  expect(dispatch).toHaveBeenCalledWith(actions.decrement())
})

test("in any case increment times", () => {
  const state = { count: 1 }
  const dispatch = jest.fn(a => {})

  sut({}, () => state, dispatch)

  expect(dispatch).toHaveBeenCalledWith(actions.incrementTime())

  state.count = 2

  sut({}, () => state, dispatch)

  expect(dispatch).toHaveBeenCalledWith(actions.incrementTime())
})
