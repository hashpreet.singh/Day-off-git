import * as actions from '../actions/counter-actions'

export default function (action, getState, dispatch) {
  const { count } = getState()

  if (count % 2 === 1) {
    dispatch(actions.decrement())
  }

  dispatch(actions.incrementTime())
}
