import * as incrementActions from './actions/counter-actions'
import previousOdd from './policies/previous-odd'
import changeCount from './policies/change-count'

const handlers = {}
try {
  handlers[incrementActions.PREVIOUS_ODD] = previousOdd

  incrementActions.allIncrements.forEach(i => (handlers[i] = changeCount))
} catch (e) {
  console.log(e)
}
export default handlers
