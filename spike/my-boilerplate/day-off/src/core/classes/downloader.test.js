import { getFileName } from './downloader'

test('it should return correct filename', () => {
  const expected = '2020-02-11_16:01:05_state.json'

  const actual = getFileName(new Date(2020, 1, 11, 16, 1, 5, 1), 'state.json')

  expect(actual).toBe(expected)
})
