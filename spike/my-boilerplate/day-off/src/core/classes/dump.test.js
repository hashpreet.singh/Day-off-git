import sut from './dump'
import initialState from '../../app/initial-state'
import * as actions from '../actions/counter-actions'

test('with no actions and no state', () => {
  const expected = {
    payload: '[]',
    preloadedState: '{}'
  }

  const actual = sut([], {})

  expect(actual).toMatchObject(expected)
})

test('with state', () => {
  const expected = {
    payload: '[]',
    preloadedState:
      '{"count":0,"times":0,"planets":{"fetching":false,"planetName":""}}'
  }

  const actual = sut([], initialState)

  expect(actual).toMatchObject(expected)
})

test('with actions', () => {
  const expected = {
    payload: '[{"type":"counter/RESET"},{"type":"counter/RESET_TIMES"}]',
    preloadedState: '{}'
  }

  const actual = sut([actions.reset(), actions.resetTimes()], {})

  expect(actual).toMatchObject(expected)
})
