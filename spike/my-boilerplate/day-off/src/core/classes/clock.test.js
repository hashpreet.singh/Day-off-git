import clock from "./clock"

test("it returns current day", () => {
  const expected = 1
  const date = new Date(2020, 1, expected, 1, 1, 1, 1)

  expect(clock(date).dayOfMonth()).toBe(expected)
})
