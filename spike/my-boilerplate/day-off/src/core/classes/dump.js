export default function dump(actions = [], state = {}) {
  return {
    payload: JSON.stringify(actions),
    preloadedState: JSON.stringify(state)
  }
}
