export default function download(filename, text) {
  const element = document.createElement('a')
  element.setAttribute(
    'href',
    `data:text/plain;charset=utf-8,${encodeURIComponent(text)}`
  )
  element.setAttribute('download', filename)

  element.style.display = 'none'
  document.body.appendChild(element)

  element.click()

  document.body.removeChild(element)
}

export function getFileName(d, suffix) {
  const day = `0${d.getDate()}`.slice(-2)
  const month = `0${d.getMonth() + 1}`.slice(-2)
  const year = d.getFullYear()
  const hours = `0${d.getHours()}`.slice(-2)
  const minutes = `0${d.getMinutes()}`.slice(-2)
  const seconds = `0${d.getSeconds()}`.slice(-2)

  return `${year}-${month}-${day}_${hours}:${minutes}:${seconds}_${suffix}`
}
