import React from 'react'
import { Provider } from 'react-redux'
import { render } from '@testing-library/react'
import store from '../../app/store'
import restAdapter from '../rest-adapter'

export default function Wrap(component, originalConfig) {
  const config = { ...(originalConfig || {}) }

  config.rest = config.rest || {}
  config.adapter = config.rest.responses
    ? restAdapter(config.rest.responses)
    : restAdapter()
  config.adapter.done = config.rest.done || (() => {})

  config.actionStorage = config.actionStorage || []

  const theStore = store.withConfig(config)

  const toReturn = render(<Provider store={theStore}>{component}</Provider>)

  toReturn.store = theStore
  toReturn.getState = theStore.getState
  toReturn.getActions = () => config.actionStorage

  return toReturn
}
