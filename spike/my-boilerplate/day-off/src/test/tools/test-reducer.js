export function testReducer(test, sut, expect) {
  return {
    shouldReturnState: () => {
      test('it must return the state if it is not a present', () => {
        const state = { something: 42 }

        const actual = sut(state, { type: 'AN action here' })

        expect(actual).toEqual(state)
      })
    },

    caseWhen: (cases, stateBuilder) => {
      test.each(cases)(
        "given the action '%p' with initial value of %p should modify value to %p",
        (action, initial, expected) => {
          const createState = value => {
            const state = stateBuilder(value)
            state.someRandomProperty = 1
            return state
          }

          const state = createState(initial)
          const expectedState = createState(expected)

          const actual = sut(state, action)

          if (!expectedState.someRandomProperty) {
            throw Error(
              'It looks that your reducer removes other state properties... have a closer look at what it is returning',
            )
          }

          expect(actual).toMatchObject(expectedState)
        },
      )
    },
  }
}
