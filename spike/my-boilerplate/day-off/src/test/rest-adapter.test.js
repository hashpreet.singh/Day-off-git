import getAdapter from "./rest-adapter"

describe("promises", () => {

  it("works with promises when they are a success", () => {
    expect.assertions(1)

    const get = getAdapter([{ data: { name: "Paul" } }])
      .get("/users/5")
      .then(user => user.name)

    return expect(get).resolves.toEqual("Paul")
  })

  it("works with async await", async () => {
    expect.assertions(1)
    const data = await getAdapter([{ data: { name: "Frank" } }])
      .get("/users/5")
      .then(user => user.name)

    expect(data).toBe("Frank")
  })

  it("works with async await and partial results", async () => {
    expect.assertions(1)

    const adapter = getAdapter([{ data: { name: "Paul" } }])
    const data = await adapter.get("/users/5").then(user => user.name)

    expect(data).toBe("Paul")
  })

  it("works with promises when they fails", () => {
    expect.assertions(1)
    const adapter = getAdapter([{ error: "error" }])

    return expect(adapter.get("/users/" + 1234)).rejects.toMatch("error")
  })
})
