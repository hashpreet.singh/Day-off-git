function createGet(responses) {
  /* eslint-disable-next-line */
  const get = url => {
    if (!responses) {
      throw TypeError('responses not defined, organize your test better noob!')
    }

    const promise = new Promise((resolve, reject) => {
      const response = responses.shift()
      return response.error ? reject(response.error) : resolve(response.data)
    })

    return promise
  }

  return get
}

const getAdapter = responses => ({
  get: createGet(responses),
})

export default getAdapter
