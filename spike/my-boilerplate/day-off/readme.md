## prerequisites

- commitizen _[website here](https://github.com/commitizen/cz-cli)_

> npm i commitizen -g

**Personalize commitizen:** Go to commitizen [index file](node_modules\conventional-commit-types\index.json) and replace its content with the [personalized one](spike\my-boilerplate\day-off\build\personal-commitizzen.json)

**tip:** every time you want to commit use: `git cz`

---

**VSCode extensions:**

- Prettier => After installing it, go to its settings and flag 'Prettier: Single quote'

- Eslint extension **EsLint by Dirk Baeumer**

## Before start

> npm i

## Running up

> The command 'gita' is present to run 'npx eslint src --fix' and then 'git add .'

> The command 'gitc' is present to run 'npx eslint src --fix' and then 'git cz'

---

## Git hook

A pre-commit git hook allows to run eslint before any commit.

**Setting up:**

Go to the folder [.git\hooks](.git\hooks) and paste the '[pre-commit](spike\my-boilerplate\day-off\build\pre-commit)' file.

---

## Styleguide

[Airbnb's styleguide](https://github.com/airbnb/javascript/tree/master/react) is used in this project, except the use of the simicolons.



## Folder structure

Folder structure is [feature based](https://redux.js.org/style-guide/style-guide/#structure-files-as-feature-folders-or-ducks). In every folder at least a component is present (most of the time it will be the only one...).

Tests are placed alongside every file and have the same name with the suffix **'.test.js'**

## Linter

**Setting-up:**

> npm i

At the end, to get our configuration, is compulsory to replace [.eslintrc](spike\my-boilerplate\day-off.eslintrc.json) file's content, with [eslintrcConfiguration](spike\my-boilerplate\day-off\build\eslintrcConfiguration)'s one.

**Running up:**

> npx eslint src --ext .jsx --ext .js .

[To make a fix use the command](https://eslint.org/docs/user-guide/configuring.html#configuring-rules):

> npx eslint src --ext .jsx --ext .js . --fix

[To escape next line from eslint](https://eslint.org/docs/user-guide/configuring.html#configuring-rules):

> /_ eslint-disable-next-line _/

To escape current line from eslint

> /_ eslint-disable-line _/

---

**WARNING** A fallback is needed to allow users to run the webpage when they don't have the [Redux Devtools Extension](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd) installed.
