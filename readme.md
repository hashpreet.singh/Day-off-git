## prerequisites

- commitizen   *[website here](https://github.com/commitizen/cz-cli)* 

> npm i commitizen -g

**Personalize commitizen:** Go to commitizen [index file](node_modules\conventional-commit-types\index.json) and replace its content with the [personalized one](spike\my-boilerplate\day-off\build\personal-commitizzen.json)

**tip:** every time you want to commit use: `git cz` 

***

- Eslint extension   **EsLint by Dirk Baeumer**

## Before start

> npm i

## Running up

> The command 'gita' is present to run 'npx eslint src --fix' and then 'git add .'


> The command 'gitc' is present to run 'npx eslint src --fix' and then 'git cz'

***

## Git hook

A pre-commit git hook allows to run eslint before any commit.

**Setting up:** 

Go to the folder [.git\hooks](.git\hooks) and paste the '[pre-commit](spike\my-boilerplate\day-off\build\pre-commit)' file.

***

## Styleguide

[Airbnb's styleguide](https://github.com/airbnb/javascript/tree/master/react) is used in this project, except the use of the simicolons. 



